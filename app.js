var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const config = require('config');
const {expressjwt} = require('express-jwt');
const i18n = require('i18n');

var indexRouter = require('./routes/index');
var membersRouter = require('./routes/members');
var recordsRouter = require('./routes/records');
var product_backlogRouter = require('./routes/product_backlog')
var permissionRouter = require('./routes/permissions');
const uri= config.get("dbChain");
var profileRouter = require('./routes/profiles');
mongoose.connect(uri);
const db = mongoose.connection;

db.on('open', ()=>{
  console.log('Conexion correcta');
});

db.on('error', ()=>{
  console.log('No se pudo conectar a la bd');
});

i18n.configure({
  locales:['es-lat', 'es-esp', 'en-eua','en-in'],
  cookie: 'language',
  directory: `${__dirname}/locales`,
});

var app = express();

//NOTAS perronas
//product backlog coleccion de las historias
//product owner es el que dice si aglo entra o no al proyecto
//SCRUM Master admin del proyecto, reuniones y que vaya "suave", y proporciona las herramientas
//Release backlog viene las historias aceptadas que si se cumplieron en el desarrollo
//Para tiempos estimados segun se dividen las tareas por horas (tareas chicas), y no pueden ser impares
//Para tiempos estimados segun se dividen las tareas por dias (tareas grandes), si no quedan, se pasa al sig mas grande
//En mamadotas se dividen en 1,2,3 o 6 meses
//Sprints de 2 a 30 dias
//Burndown es la grafiquilla pitera de work remaining y y time x

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.init);

const jwtKey = config.get("secret.key");
//app.use(expressjwt({secret:jwtKey,algorithms:['HS256']})
  //.unless({path:['/login']}));

app.use('/', indexRouter);
app.use('/members',membersRouter);
app.use('/records', recordsRouter);
app.use('/product_backlog',  product_backlogRouter);
app.use('/permissions', permissionRouter);
app.use('/profiles', profileRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use((req, res, next) => {
  req.ability = abilities.defineRulesFor(req.auth.data);
  next();
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
