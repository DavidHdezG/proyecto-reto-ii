const supertest = require('supertest');

const app = require('../app');

//Sentencia

describe("Probar el sistema de miembros", () => {
    it("Deberia de obtener un login con usuario y contraseña correctos", (done)=>{
        supertest(app)
        .post('/members')
        .send({'idMember':'1', 'fullName':'Dante rodriguez', 'dateOfBirth':'27/03/2001',
                'curp':'ROHD270301HCHDRNA7','rfc':'ROHD270301HCHDR08','address.number':'17191','address.zip':'12125',
                'address.state':'Chihuahua','address.city':'Chihuahua','ranked':'Junior', 'language':'C#'})
        .expect(200)
        .end(function(err, res){
            if(err) {
                done(err);
            } else {
                done();
            }
        });
    });

    it("No deberia de obtener un login con usuario y contraseña incorrectos", (done)=>{
        supertest(app)
        .post('/login')
        .send({'idMember':'100', 'fullName':'Diego Rodriguez', 'dateOfBirth':'26/10/2008',
                'curp':'ROHD261008HCHHC3','rfc':'ROHD261008HCH09','address.number':'17917','address.zip':'91123',
                'address.state':'Chihuahua','address.city':'Delicias','ranked':'Junior', 'language':'C++'})
        .expect(403)
        .end(function(err, res){
            if(err) {
                done(err);
            } else {
                done();
            }
        });
    });
});