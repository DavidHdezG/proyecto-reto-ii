const supertest = require('supertest');

const app = require('../app');

//Sentencia

describe("Probar el sistema de historias de usuario", () => {
    it("Deberia de obtener una historia de usuario correcta", (done)=>{
        supertest(app)
        .post('/records')
        .send({'idFiles':'1', 'projectName':'Web', 'requestDate':'18/11/2022',
                'projectStartDate':'30/11/2022','projectDescription':'Parcial 2 y 3','projectManager':'David'
                ,'benefit':'Buenas califas','shortNameBack':'Proyectazo','context':'Pal aulas','events':'Pasar', 'results':'Un 10'})
        .expect(200)
        .end(function(err, res){
            if(err) {
                done(err);
            } else {
                done();
            }
        });
    });

    it("No deberia de obtener un login con usuario y contraseña incorrectos", (done)=>{
        supertest(app)
        .post('/records')
        .send({'idFiles':'99', 'projectName':'Bases', 'requestDate':'15/7/2020',
        'projectStartDate':'16/8/2020','projectDescription':'Parcial 2 y 3','projectManager':'David'
        ,'benefit':'Buenas califas','shortNameBack':'Proyectazo','context':'Pal aulas','events':'Pasar', 'results':'Un 10'})
        .expect(403)
        .end(function(err, res){
            if(err) {
                done(err);
            } else {
                done();
            }
        });
    });
});