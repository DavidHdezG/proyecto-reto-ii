const supertest = require('supertest');

const app = require('../app');

//Sentencia

describe("Probar el sistema de historias de usuario", () => {
    it("Deberia de obtener una historia de usuario correcta", (done)=>{
        supertest(app)
        .post('/product_backlog')
        .send({'priority':'2', 'size':'10', 'showrtNameFront':'Controllers',
                'rol':'Master','funtionality':'Backend','benefit':'Que funcion',
                'shortNameBack':'Models','context':'Idioma español e ingles','events':'Change language', 'results':'pal backlog'})
        .expect(200)
        .end(function(err, res){
            if(err) {
                done(err);
            } else {
                done();
            }
        });
    });

    it("Esta ", (done)=>{
        supertest(app)
        .post('/product_backlog')
        .send({'priority':'5', 'size':'10', 'shortNameFront':'Routes',
                'rol':'Junior','funtionality':'Backend','benefit':'connection between routes',
                'shortNameBack':'Routes','context':'Conexiones','events':'Cambiar conexiones', 'results':'Conexiones exitosas'})
        .expect(403)
        .end(function(err, res){
            if(err) {
                done(err);
            } else {
                done();
            }
        });
    });
});