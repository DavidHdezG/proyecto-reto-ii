const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _idMember: Number,
    _fullName:String,
    _dateOfBirth:Date,
    _curp:String,
    _rfc:String,
    _address: {
        street: String,
        number: String,
        zip: Number,
        state: String,
        city: String,
    },
    _ranked:{
        type:String,
        enum : ['JUNIOR','SENIOR','MASTER'],
    },
    _language:{
        type:String,
        enum : ['JAVA','JAVASCRIPT','C++','PYTHON','C#']
    },
    _profiles:[{
        type: mongoose.Schema.ObjectId,
        ref: 'Profile'
    }]
});

class Member{
    constructor(idMember, fullName, dateOfBirth, curp, rfc, address){
        this._idMember = idMember;
        this._fullName = fullName;
        this._dateOfBirth = dateOfBirth;
        this._curp = curp;
        this._rfc = rfc;
        this._address = address;
    }

    get idMember(){
        return this._idMember;
    }

    set idMember(idMember){
        this._idMember = idMember;
    }

    get fullName(){
        return this._fullName;
    }

    set fullName(fullName){
        this._fullName = fullName;
    }

    get dateOfBirth(){
        return this._dateOfBirth;
    }

    set dateOfBirth(dateOfBirth){
        this._dateOfBirth = dateOfBirth;
    }

    get curp(){
        return this._curp;
    }

    set curp(curp){
        this._curp = curp;
    }

    get rfc(){
        return this._rfc;
    }

    set rfc(rfc){
        this._rfc = rfc;
    }

    get address(){
        return this._address;
    }

    set address(address){
        this._address = address;
    }
}



schema.loadClass(Member);
module.exports = mongoose.model("Member",schema);
