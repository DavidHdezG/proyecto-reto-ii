//Estos son los proyectos

const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _idFiles: Number,
    _projectName: String,
    _projectRequestDate: Date,
    _projectStartDate: Date,
    _projectDescription: String,
    _projectManager: {
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    },
    _projectOwner: {
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    },
    _projectMembers: [{
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    }],
});

class File{
    constructor(idFiles, projectName, projectRequestDate, projectStartDate, projectDescription, projectManager, projectOwner, projectMembers){
        this._idFiles = idFiles;
        this._projectName = projectName;
        this._projectRequestDate = projectRequestDate;
        this._projectStartDate = projectStartDate;
        this._projectDescription = projectDescription;
        this._projectManager = projectManager;
        this._projectOwner = projectOwner;
        this._projectMembers = projectMembers;
    }

    get idFiles(){
        return this._idFiles;
    }

    set idFiles(idFiles){
        this._idFiles = idFiles;
    }

    get projectName(){
        return this._projectName;
    }

    set projectName(projectName){
        this._projectName = projectName;
    }

    get projectRequestDate(){
        return this._projectRequestDate;
    }

    set projectRequestDate(projectRequestDate){
        this._projectRequestDate = projectRequestDate;
    }

    get projectStartDate(){
        return this._projectStartDate;
    }

    set projectStartDate(projectStartDate){
        this._projectStartDate = projectStartDate;
    }

    get projectDescription(){
        return this._projectDescription;
    }

    set projectDescription(projectDescription){
        this._projectDescription = projectDescription;
    }

    get projectManager(){
        return this._projectManager;
    }

    set projectManager(projectManager){
        this._projectManager = projectManager;
    }

    get projectOwner(){
        return this._projectOwner;
    }

    set projectOwner(projectOwner){
        this._projectOwner = projectOwner;
    }

    get projectMembers(){
        return this._projectMembers;
    }

    set projectMembers(projectMembers){
        this._projectMembers = projectMembers;
    }
}

schema.loadClass(File);
module.exports = mongoose.model('File', schema);