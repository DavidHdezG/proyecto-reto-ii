//Estas son las user stories

const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _priority: String,
    _size: String,
    _shortNameFront: String,
    _rol: String,
    _functionality: String,
    _benefit: String,
    _shortNameBack: String,
    _context: String,
    _events: [String],
    _results:[String]
});

class Record{
    constructor(priority, size, shortNameFront, rol, functionality, benefit, shortNameBack, context, events, results){
        this._priority = priority;
        this._size = size;
        this._shortNameFront = shortNameFront;
        this._rol = rol;
        this._functionality = functionality;
        this._benefit = benefit;
        this._shortNameBack = shortNameBack;
        this._context = context;
        this._events = events;
        this._results = results;
    }

    get priority(){
        return this._priority;
    }

    set priority(priority){
        this._priority = priority;
    }

    get size(){
        return this._size;
    }

    set size(size){
        this._size = size;
    }

    get shortNameFront(){
        return this._shortNameFront;
    }

    set shortNameFront(shortNameFront){
        this._shortNameFront = shortNameFront;
    }

    get rol(){
        return this._rol;
    }

    set rol(rol){
        this._rol = rol;
    }

    get functionality(){
        return this._functionality;
    }

    set functionality(functionality){
        this._functionality = functionality;
    }

    get benefit(){
        return this._benefit;
    }

    set benefit(benefit){
        this._benefit = benefit;
    }

    get shortNameBack(){
        return this._shortNameBack;
    }

    set shortNameBack(shortNameBack){
        this._shortNameBack = shortNameBack;
    }

    get context(){
        return this._context;
    }

    set context(context){
        this._context = context;
    }

    get events(){
        return this._events;
    }

    set events(events){
        this._events = events;
    }

    get results(){
        return this._results;
    }

    set results(results){
        this._results = results;
    }
}

schema.loadClass(Record);
module.exports = mongoose.model('Record', schema);
