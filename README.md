# Proyecto reto II

Dante Yahir Rodríguez Herrera - 338725 <br>
José Carlos Chaparro Morales - 329613 <br>
Luis Raúl Chacón Muñoz - 339011 <br>
David Eduaro Hernández García - 338953 <br>

# Diagrama de clases
![Diagrama de secuencia](./public/images/Diagrama_de_clases.png) <br>
Esta compuesto de 3 clases, una para los proyectos, de la cual tiene atributos individuales, y dos referencias; luego la segunda es de los miembros del proyecto con atributos individuales, un embebido para la direccion y una lista de compuesta de dos partes, lenguajes y nivel, y finalmente la de las historias que esta compuesta de solamente atributos individuales.

# Diagrama de secuencia
![Diagrama de secuencia](./public/images/Diagrama_secuencia.png) <br>
Describe el proceso que necesita el SCRUM desde el backlog incluyendo los release de cada uno, y también la retrospectiva para cada release del como haya resultado la user story, para al final hacer el cierre de proyecto.

