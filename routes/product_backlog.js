//Cuando creemos models ligarlo con userStory.js 
//product_backlog es la coleccion de user stories

var express = require('express');
var router = express.Router();

const controller = require("../controllers/product_backlog");

/* GET users listing. */
router.get('/', controller.list);

router.get('/:id', controller.index);

router.post('/', controller.create);

router.put('/:id', controller.replace);

router.patch('/:id', controller.update);

router.delete('/:id', controller.destroy);



module.exports = router;