const { AbilityBuilder, PureAbility } = require("@casl/ability");
const Member = require("../models/member");
const mongoose = require("mongoose");

async function defineAbilitiesFor(member) {
  const { can, cannot, rules } = new AbilityBuilder(PureAbility);

  if(!member){
    return new PureAbility(rules);
  }

  let permissions = await Member.aggregate([
    {$match: {"_id":_idMember}},
    {$lookup:{
        from:"profiles",
        localField:"_profiles",
        foreignField:"_id",
        as:"profiles"
        }
    },
    {$unwind:"$profiles"},
    {$lookup:{
        from:"permissions",
        localField:"profiles._permissions",
        foreignField:"_id",
        as:"permissions"
        }
    },
    {$project:{
        "permissions":1,
    }}
  ])

  const availablePermissions = permissions.map(permission => permission.permissions);

    
    for(let permission of availablePermissions){
        can(permission._type,mongoose.model(permission._description));
    }

    return new PureAbility(rules);
}

module.exports = defineAbilitiesFor;