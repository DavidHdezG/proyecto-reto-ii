//A partir de user stories, se crea el controlador
//pvta
const express = require("express");
const File = require("../models/file");
const Member = require("../models/member");
const { ForbiddenError } = require("@casl/ability");
//GET /users/  list
function list(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("read", "ProductBacklog");
    File.find()
      .then((objs) =>
        res.status(200).json({
          message: res.__("ok.list"),
          obj: objs,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.list"),
          obj: ex,
        })
      );
  });
}

//GET /users/{id}  index
function index(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("read", "ProductBacklog");
    const _id = req.params.idFiles;
    Copy.findOne({ _id: id })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.index"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.index"),
          obj: ex,
        })
      );
  });
}

//POST /users/  create
function create(req, res, next) {
  req.ability.then(async (ability) => {
    const priority = req.body.priority;
    const size = req.body.size;
    const shortNameFront = req.body.shortNameFront;
    const rol = req.body.rol;
    const functionality = req.body.functionality;
    const benefit = req.body.benefit;
    const shortNameBacka = req.body.shortNameBack;
    const contexta = req.body.context;

    let shortNameBack = await Member.findOne({ _id: idMember });
    let context = await Member.findOne({ _id: idMember });

    let file = new File({
      priority: priority,
      size: size,
      shortNameFront: shortNameFront,
      rol: rol,
      functionality: functionality,
      benefit: benefit,
      shortNameBack: shortNameBack,
      context: context,
    });

    file
      .save()
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.create"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.create"),
          obj: ex,
        })
      );
  });
}

//PUT /users/{id}  replace
function replace(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("update", "ProductBacklog");
    const idFiles = req.body.idFiles;

    let priority = req.body.priority ? req.body.priority : "";
    let size = req.body.size ? req.body.size : "";
    let shortNameFront = req.body.shortNameFront ? req.body.shortNameFront : "";
    let rol = req.body.rol ? req.body.rol : "";
    let benefit = req.body.benefit ? req.body.benefit : "";
    let shortNameBack = req.body.idMember ? req.body.idMember : "";
    let context = req.body.idMember ? req.body.idMember : "";

    let file = new File({
      idFiles: idFiles,
      size: size,
      shortNameFront: shortNameFront,
      rol: rol,
      benefit: benefit,
      shortNameBack: shortNameBack,
      context: context,
      projectMembers: projectMembers,
    });

    file
      .findOneAndUpdate({ _id: id }, genre, { new: true })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.replace"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.replace"),
          obj: ex,
        })
      );
  });
}

//PATCH /users/{id} update
function update(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("update", "ProductBacklog");
    const _id = req.body.idFiles;

    let size = req.body.size;
    let shortNameFront = req.body.shortNameFront;
    let rol = req.body.rol;
    let benefit = req.body.benefit;
    let shortNameBack = req.body.idMember;
    let context = req.body.idMember;
    let file = new Object();

    if (size) {
      file._size = size;
    }

    if (shortNameFront) {
      file._projectRequesDate = shortNameFront;
    }

    if (rol) {
      file._rol = rol;
    }

    if (benefit) {
      file._benefit = benefit;
    }

    if (shortNameBack) {
      file._shortNameBack = shortNameBack;
    }

    if (context) {
      file._context = context;
    }

    file
      .findOneAndUpdate({ _id: id }, genre, { new: true })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.update"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.update"),
          obj: ex,
        })
      );
  });
}

//DELETE /users/{id} destroy
function destroy(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("delete", "ProductBacklog");
    const _id = req.params.id;
    Copy.remove({ _id: id })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.delete"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.delete"),
          obj: ex,
        })
      );
  });
}

module.exports = { list, index, create, replace, update, destroy };
