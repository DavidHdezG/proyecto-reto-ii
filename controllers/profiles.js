const express = require('express');
const { ForbiddenError } = require('@casl/ability');
const Profile = require('../models/profile');
const Permission = require('../models/permission');
function list(req, res, next) {
    req.ability.then(
        ability => {
            ForbiddenError.from(ability).throwUnlessCan('read', Profile);
            Profile.find().then(objs => res.status(200).json({
                message: res.__('ok.list'),
                obj: objs
            })).catch(ex => res.status(500).json({
                message: res.__('bad.list'),
                obj: ex
            }));
        }
    )
}

function index(req, res, next) {
    const id = req.params.id;
    req.ability.then(
        ability => {
            ForbiddenError.from(ability).throwUnlessCan('read', Profile);
            Profile.findOne({ "_id": id }).then(obj => res.status(200).json({
                message: res.__('ok.index'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.index'),
                obj: ex
            }));
        }
    )
}

function create(req,res,next){
    req.ability.then(
        async ability => {
            ForbiddenError.from(ability).throwUnlessCan('create', Profile);
            const description = req.body.description;
            const status = req.body.status;
            const permissionId = req.body.permissionId;
            
            let permissions = await Permission.find({ "_id": { $in: permissionId } });
            let profile = new Profile({
                description: description,
                status: status,
                permissions: permissions
            });

            profile.save().then(obj => res.status(200).json({
                message: res.__('ok.create'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.create'),
                obj: ex
            }));
        }
    )
}

function replace(req,res,next){
    req.ability.then(
        async ability => {
            ForbiddenError.from(ability).throwUnlessCan('update', Profile);
            const id = req.params.id;
            const description = req.body.description ? req.body.description : null;
            const status = req.body.status ? req.body.status : null;
            const permissionId = req.body.permissionId? req.body.permissionId : [];
            let permissions = await Permission.find({ "_id": { $in: permissionId } });
            let profile = new Object({
                _description: description,
                _status: status,
                _permissions: permissions
            });

            Profile.findOneAndUpdate({ "_id": id}, profile, { new: true }).then(obj => res.status(200).json({
                message: res.__('ok.replace'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.replace'),
                obj: ex
            }));
        })       
}

function update(req,res,next){
    req.ability.then(
        async ability => {
            ForbiddenError.from(ability).throwUnlessCan('update', Profile);
            const id = req.params.id;
            const description = req.body.description ? req.body.description : null;
            const status = req.body.status ? req.body.status : null;
            const permissionId = req.body.permissionId? req.body.permissionId : [];
            let permissions = await Permission.find({ "_id": { $in: permissionId } });
            let profile = new Object({
                _description: description,
                _status: status,
                _permissions: permissions
            });

            Profile.findOneAndUpdate({ "_id": id}, profile, { new: true }).then(obj => res.status(200).json({
                message: res.__('ok.update'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.update'),
                obj: ex
            }));
        })
}

function destroy(req,res,next){
    req.ability.then(
        ability => {
            ForbiddenError.from(ability).throwUnlessCan('delete', Profile);
            const id = req.params.id;
            Profile.findOneAndDelete({ "_id": id }).then(obj => res.status(200).json({
                message: res.__('ok.destroy'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.destroy'),
                obj: ex
            }));
        }
    )
}

module.exports = { list, index, create, replace, update, destroy };