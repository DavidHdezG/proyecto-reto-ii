//Gei
// si soi

const express = require("express");
const Permission = require("../models/permission");
const { ForbiddenError } = require("@casl/ability");

function list(req, res, next) {
  Permission.find()
    .then((objs) =>
      res.status(200).json({
        message: res.__("ok.list"),
        obj: objs,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: res.__("bad.list"),
        obj: ex,
      })
    );
}

function index(req, res, next) {
  const idPermission = req.params.idPermission;
  Permission.findOne({ _id: idPermission })
    .then((obj) =>
      res.status(200).json({
        message: res.__("ok.index"),
        obj: obj,
      })
    )
    .catch((ex) =>
      res.status(500).json({
        message: res.__("bad.index"),
        obj: ex,
      })
    );
}

function create(req, res, next) {
  req.ablity.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("create", "Permission");
    const description = req.body.description;
    const type = req.body.type;

    let permission = new Permission({
      description: description,
      type: type,
    });

    permission
      .save()
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.create"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.create"),
          obj: ex,
        })
      );
  });
}

function replace(req, res, next) {
    req.ability.then(
        async ability => {
            ForbiddenError.from(ability).throwUnlessCan('update', 'Permission');
            const id = req.params.id;
            const description = req.body.description?req.body.description:null;
            const type = req.body.type?req.body.type:null;
            Permission.findOneAndUpdate({ "_id": id }, { $set: { description: description, type: type } }, { new: true }).then(obj => res.status(200).json({
                message: res.__('ok.replace'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.replace'),
                obj: ex
            }));
        }
    )
}

function update(req, res, next) {

    req.ability.then(
        async ability => {
            ForbiddenError.from(ability).throwUnlessCan('update', 'Permission');
            const id = req.params.id;
            const description = req.body.description;
            const type = req.body.type;
            Permission.findOneAndUpdate({ "_id": id }, { $set: { description: description, type: type } }, { new: true }).then(obj => res.status(200).json({
                message: res.__('ok.replace'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.replace'),
                obj: ex
            }));
        }
    )
}

function destroy(req, res, next) {
    req.ability.then(
        async ability => {
            ForbiddenError.from(ability).throwUnlessCan('delete', 'Permission');
            const id = req.params.id;
            Permission.findOneAndDelete({ "_id": id }).then(obj => res.status(200).json({
                message: res.__('ok.destroy'),
                obj: obj
            })).catch(ex => res.status(500).json({
                message: res.__('bad.destroy'),
                obj: ex
            }));
        }
    )
}
module.exports = { list, index, create, replace, update, destroy };
