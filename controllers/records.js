//A partir de file, se crea el controlador
//pvta
const express = require('express');
const File = require('../models/file');
const Member = require('../models/member');
const { ForbiddenError } = require("@casl/ability");
//GET /users/  list
function list(req, res, next) {
  req.ability.then(
    ability => {
        ForbiddenError.from(ability).throwUnlessCan('read', 'Member');
        File.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
        })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
    }
  )
  
};

//GET /users/{id}  index
function index(req, res, next) {
  req.ability.then(
    ability => {
      ForbiddenError.from(ability).throwUnlessCan('read', 'Member');
      const idFiles = req.params.idFiles;
      Copy.findOne({"_id":id}).then(obj => res.status(200).json({
          message: res.__('ok.index'),
          obj:obj
      })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
          obj: ex
      }));
    }
  )
};

//POST /users/  create
 function create(req, res, next) {
  req.ability.then(
    async ability => {
      ForbiddenError.from(ability).throwUnlessCan('create', 'Member');
      const idFiles = req.body.idFiles;
      const projectName = req.body.projectName;
      const projectRequestDate = req.body.projectRequestDate;
      const projectStartDate = req.body.projectStartDate;
      const projectDescription = req.body.projectDescription;

      let projectManager = await Member.findOne({"_id": idMember});
      let projectOwner = await Member.findOne({"_id": idMember});
      let projectMembers = await Member.findOne({"_id": idMember});

      let file = new File({
          idFiles: idFiles,
          projectName: projectName,
          projectRequestDate: projectRequestDate,
          projectStartDate: projectStartDate,
          projectDescription: projectDescription,
          projectManager: projectManager,
          projectOwner: projectOwner,
          projectMembers: projectMembers
      });

      file.save().then(obj => res.status(200).json({
          message: res.__('ok.create'),
          obj: obj
      })).catch(ex => res.status(500).json({
          message: res.__('bad.create'),
          obj: ex
      }));     
    }
  )
  
}

//PUT /users/{id}  replace
function replace(req, res, next) {
  req.ability.then(
    ability => {
      ForbiddenError.from(ability).throwUnlessCan('update', 'Member');
      const idFiles= req.body.idFiles;

        let projectName = req.body.projectName ? req.body.projectName : "";
        let projectRequestDate = req.body.projectRequestDate ? req.body.projectRequestDate : "";
        let projectStartDate = req.body.projectStartDate ? req.body.projectStartDate : "";
        let projectDescription = req.body.projectDescription ? req.body.projectDescription : "";
        
        let projectManager = req.body.idMember ? req.body.idMember : "";
        let projectOwner = req.body.idMember ? req.body.idMember : "";

        let file = new File({
          idFiles: idFiles,
          projectName: projectName,
          projectRequestDate: projectRequestDate,
          projectStartDate: projectStartDate,
          projectDescription: projectDescription,
          projectManager: projectManager,
          projectOwner: projectOwner,
          projectMembers: projectMembers
      });

        file.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => res.status(200).json({
          message: res.__('ok.replace'),
            obj:obj
        })).catch(ex => res.status(500).json({
          message: res.__('bad.replace'),
            obj: ex
        }));
    }
  )
  
}

//PATCH /users/{id} update
function update(req, res, next) {
  req.ability.then(
    ability => {
      ForbiddenError.from(ability).throwUnlessCan('update', 'Member');
      const idFiles= req.body.idFiles;

      let projectName = req.body.projectName;
      let projectRequestDate = req.body.projectRequestDate;
      let projectStartDate = req.body.projectStartDate;
      let projectDescription = req.body.projectDescription;
      
      let projectManager = req.body.idMember;
      let projectOwner = req.body.idMember;

      let file = new Object();

      if(projectName){
        file._projectName=projectName;
      }

      if(projectRequestDate){
        file._projectRequesDate=projectRequestDate;
      }

      if(projectStartDate){
        file._projectStartDate=projectStartDate;
      }

      if(projectDescription){
        file._projectDescription=projectDescription;
      }

      if(projectManager){
        file._projectManager=projectManager;
      }

      if(projectOwner){
        file._projectOwner=projectOwner;
      }

      file.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => res.status(200).json({
          message: res.__('ok.update'),
          obj:obj
      })).catch(ex => res.status(500).json({
          message: res.__('bad.update'),
          obj: ex
      }));
    }
  )
  
}

//DELETE /users/{id} destroy
function destroy(req, res, next) {
  req.ability.then(
    ability => {
      ForbiddenError.from(ability).throwUnlessCan('delete', 'Member');
      const id = req.params.id;
      Copy.remove({"_id":id}).then(obj => res.status(200).json({
          message: res.__('ok.delete'),
          obj:obj
      })).catch(ex => res.status(500).json({
          message: res.__('bad.delete'),
          obj: ex
      }));
    }
  )
  
  }

module.exports = { list, index, create, replace, update, destroy };