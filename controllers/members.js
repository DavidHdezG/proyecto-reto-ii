const express = require("express");
const Member = require("../models/member");
const { ForbiddenError } = require("@casl/ability");

function list(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("read", "Member");
    Member.find()
      .then((objs) =>
        res.status(200).json({
          message: res.__("ok.list"),
          obj: objs,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.list"),
          obj: ex,
        })
      );
  });
}

function index(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("read", "Member");
    const id = req.params.id;
    Member.findOne({ _id: id })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.index"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.index"),
          obj: ex,
        })
      );
  });
}

function create(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("create", "Member");
    const fullname = req.body.fullname;

    let address = new Object();
    address.street = req.body.street;
    address.number = req.body.number;
    address.zip = req.body.zip;
    address.state = req.body.state;

    const phone = req.body.phone;

    let member = new Member({
      fullname,

      phone,
      address,
    });

    member
      .save()
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.create"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.create"),
          obj: ex,
        })
      );
  });
}

function replace(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("replace", "Member");
    const id = req.params.id;

    const fullname = req.body.fullname ? req.body.fullname : "";

    let address = new Object();
    address.street = req.body.street ? req.body.street : "";
    address.number = req.body.number ? req.body.number : "";
    address.zip = req.body.zip ? req.body.zip : 0;
    address.state = req.body.state ? req.body.state : "";
    const phone = req.body.phone ? req.body.phone : "";

    let member = new Object({
      _fullname: fullname,

      _phone: phone,
      _address: address,
    });
    console.log(member);

    Member.findOneAndUpdate({ _id: id }, member, { new: true })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.replace"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.replace"),
          obj: ex,
        })
      );
  });
}

function update(req, res, next) {
  req.ability.then((ability) => {
    const id = req.params.id;

    const fullname = req.body.fullname;

    const street = req.body.street;
    const number = req.body.number;
    const zip = req.body.zip;
    const state = req.body.state;

    const phone = req.body.phone;

    let member = new Object();

    let address = new Object();

    if (fullname) {
      member._fullname = fullname;
    }

    if (street) {
      address.street = street;
    }

    if (number) {
      address.number = number;
    }

    if (zip) {
      address.zip = zip;
    }

    if (state) {
      address.state = state;
    }

    member._address = address;

    if (phone) {
      member._phone = phone;
    }

    Member.findOneAndUpdate({ _id: id }, member, { new: true })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.update"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.update"),
          obj: ex,
        })
      );
  });
}

function destroy(req, res, next) {
  req.ability.then((ability) => {
    ForbiddenError.from(ability).throwUnlessCan("delete", "Member");
    const id = req.params.id;
    Member.remove({ _id: id })
      .then((obj) =>
        res.status(200).json({
          message: res.__("ok.delete"),
          obj: obj,
        })
      )
      .catch((ex) =>
        res.status(500).json({
          message: res.__("bad.delete"),
          obj: ex,
        })
      );
  });
}

module.exports = { list, index, create, replace, update, destroy };
