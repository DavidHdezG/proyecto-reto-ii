FROM node
LABEL Jose Carlos Chaparro Morales - 329613, Dante Yahir Rodriguez Herrera - 338725, David Eduardo Hernández García - 338953, Luis Raúl Chacón Muñoz - 339011 
WORKDIR /app
COPY . .
ENV HOME Microproyecto
RUN npm install
EXPOSE 3000
CMD npm start
